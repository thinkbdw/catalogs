Catalog Documentation: [https://docs.catalog.style/](https://docs.catalog.style/)

# Installation

First you're going to want to install all of the node modules required for the project. You're going to need a version of node installed whch is 10.16.2 or above. Then you'll want to navigate to the specific catalog folder e.g countryside and run the below command.

```bash
npm install
```

## Preview the catalog

To start the local environment for the catalog to be able to preview it in the browser, you'll need to start the local server.

```bash
npm run catalog-start
```

This should start up the environment on port 4000 and can be accessed via [http://localhost:4000](http://localhost:4000)

## Editing the catalog

Each catalog consists of an `index.js` file which houses the styling and configuration of the catalog. The pages are split out into separate markdown files.

Catalog has a number of 'Specimens' which can be put on a page, like a library of different elements. Specimens are blocks of code that display different things.
